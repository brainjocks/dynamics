Local Environment Setup
=======================

## Prerequisites

* Visual Studio 2017
* Sitecore 9 update 2
* Sitecore powershell extensions module(latest version)
* Sitecore TDS 5.7 and up (http://www.teamdevelopmentforsitecore.com/Download)
* Powershell 5.1
* Microsoft .Net Framework 4.6.2
* nodejs and SASS 
* Sql Server 2016 SP1 
* Microsoft Visual C++ 2015 Redistributable
* SQL Server System CLR Types from the Prerequisites list above
* Microsoft Web Platform Installer 5.0
   - Install: 
   - URL Rewrite 2.1
   - Web Deploy 3.6 for Hosting Servers
   - Microsoft® SQL Server® Data-Tier Application Framework (17.1 DacFx) if you can not find it over the Web Platform then install it from the Prerequisites list above

**Clone Git Repo**
* Create a folder C:\Projects and then under that add a folder called dynamics so you should now have path called C:\Projects\dynamics ( You can do it differently as long as you understand what you are doing)
* Open a DOS window as Administrator
* Type "cd C:\Projects\dynamics" (without double quotes)
* git clone git@bitbucket.org:brainjocks/dynamics.git
* Use the develop branch

## Install Solr 6.6.2
- Download JRE from the Prerequisites list above (this might not be needed if you already have Solr installed)
 - Install JRE
 - Add a system environment variable called JAVA_HOME that points to your java installation:
   - Variable Name: JAVA_HOME
   - Variable Value (set as necessary): C:\Program Files (x86)\Java\jre1.8.0_77
- Download Solr 6.6.2 from the Prerequisites list above
- Create a folder called C:\solr-6.6.2
- Unzip the Solr zip file and place it in C:\solr-6.6.2
  - Hint: Remove any intermediary folders to avoid a folder structure like C:\solr-6.6.2\solr-6.6.2\server
  - It should now be a valid path on your file system: C:\solr-6.6.2\server
- Download NSSM from the Prerequisites list above
  - Unzip it into C:\nssm
  - Open a DOS Window with Administrator rights
  - Type "powershell" (without double quotes)
  - cd C:\nssm\win64
  - ./nssm install "SOLR"
  - The NSSM Service Installer application window will open. Set the following values:
    - Path: C:\solr-6.6.2\bin\solr.cmd
    - Startup Directory: C:\solr-6.6.2\bin
    - Arguments: start -f -p 8983
    - Display Name: SOLR
    - Description: C:\solr-6.6.2\bin\solr.cmd start -f -p 8983
    - Startup type: Automatic
    - Open your services and start the newly created "SOLR" service
- Verify that you can see the SOLR Dashboard at http://localhost:8983/solr/#/
- Reboot your computer and then verify that you can see SOLR Dashboard at http://localhost:8983/solr/#/
  - If you can't see it then make sure your Startup Type in Services for "SOLR" is set to Automatic
  - Also make sure that if you have another Solr instance installed that is using the same port (8983) that you stop that service and set it to Manual in Services
  
## SOLR SSL SETUP
- Open a DOS window in Admin mode
- Type "powershell" (without double quotes)
- cd C:\Projects\dynamics
- .\Automation\solr-ssl\solrssl.ps1
  - If you run into the following error message
    - keytool.exe not on path. Enter path to keytool (found in JRE bin folder)
  - You can enter something like this C:\Program Files\Java\jdk1.8.0_77\jre\binkeytool.exe if you have the same jdk version as mine
- Once that runs it will create 2 files under C:\Projects\dynamics\Automation\solr-ssl\ and copy them to C:\solr-6.6.2\server\etc
  - solr-ssl.keystore.jks
  - solr-ssl.keystore.p12
- Open C:\solr-6.6.2\bin\solr.in.cmd and add the lines below to it
```
set SOLR_SSL_KEY_STORE=etc/solr-ssl.keystore.jks
set SOLR_SSL_KEY_STORE_PASSWORD=secret
set SOLR_SSL_TRUST_STORE=etc/solr-ssl.keystore.jks
set SOLR_SSL_TRUST_STORE_PASSWORD=secret
```

- Make sure that the certificate solr-ssl.keystore.p12 is added to the trusted root certificates
- Restart "SOLR" window service
- If it works then you should be able to go to https://localhost:8983/solr/#/ but it also means that http://localhost:8983/solr/#/ will no longer work. (HTTPS is what has been added)

## Install SIF
- Open Windows PowerShell (run as administrator)
- Use the following Powershell commands to install the latest version of SIF
- After running the commands below make sure the modules are under C:\Program Files\WindowsPowerShell\Modules and you should see 2 folders
  - SitecoreFundamentals
  - SitecoreInstallFramework
- If you do NOT see these 2 folders then it might have installed it under C:\Program Files (x86)\WindowsPowerShell\Modules which means the version of Powershell you used was x86 or 32 bit and you need to use 64 bit Powershell

```
Register-PSRepository -Name SitecoreGallery -SourceLocation https://sitecore.myget.org/F/sc-powershell/api/v2
Install-Module SitecoreInstallFramework
Install-Module SitecoreFundamentals
Update-Module SitecoreInstallFramework
Update-Module SitecoreFundamentals
Import-Module SitecoreFundamentals -Force
Import-Module SitecoreInstallFramework -Force
```

## Enable Contained Database Authentication
Open Sql Server Management Studio (make sure you did this under the SQL Server 2016 instance in case you still have an earlier version installed as well)
Open a new query window and run this script 
```
EXEC sp_configure 'contained database authentication', 1;
GO
RECONFIGURE;
GO
```

## Install Sitecore 9 Update 2
- Copy your Sitecore License file (license.xml) to the folder you want to install your Sitecore 9 site. The path could be C:\Projects\dynamics
- Also coppy your Sitecore License file to tests folder. FakeDB is used for testing and it requires a valid Sitecore License. 
- Copy `Sitecore 9.0.2 rev. 180604 (WDP XP0 packages).zip` package to C:\Projects\dynamics
- Double check that the install.ps1 script exists under C:\Projects\dynamics\Automation\Sitecore-Versions\9.0\
- Open Windows Powershell as Administrator
- Type "cd C:\Projects\dynamics" (without the double quotes)
- Run the powershell script below
  - This is a sample script so please change the variables values that fit your instance
  
```
.\Automation\Sitecore-Versions\9.0\install.ps1 -xp0package "Sitecore 9.0.2 rev. 180604 (WDP XP0 packages).zip" -site "dynamics" -solrUrl "https://localhost:8983/solr" -solrRoot "C:\solr-6.6.2\" -solrService "SOLR" -sqlServer "DESKTOP-VMCOPY\SQLSERVER2016" -sqlAdminUser "sa" -sqlAdminPassword "<Your Password>" -installDirectory ".\sandbox\" -license ".\license.xml"
```
- This can take a while to run so please be patient :-)
- Once it is done and you didn't run into any errors then check that you can log into Sitecore (http://dynamics/sitecore/login)
- If you ran into some errors then scroll down to the Troubleshooting section to see if there is already a known fix for your issue
- Troubleshooting
  - If you have Windows 10 and you don't have Powershell 5.1 installed then you might run into an error so you will need to do the following 
  - Open C:\Projects\dynamics\Automation\Sitecore-Versions\9.0\install.ps1
 - Remove at Line 1: #Requires -Version 5.1
 - Save the change and try it again

## Install Sitecore Powershell Extensions Module
- One of the items to download from the list above was Powershell Extensions so please download it if you haven't already
- Log into Sitecore http://dynamics/sitecore/login)
- Click the "Desktop" icon
- Click the Sitecore icon button > Development Tools > Installation Wizard
- Find the Powershell Extensions package you downloaded and install it
- Once done it will show a Close button and will ask to restart the client, click Close and you will be done

## Install SCORE
- Add the score.license to the following area (C:\Projects\dynamics\sandbox\dynamics\App_Data) and then restart IIS
- Open Visual Studio 2017 and open the solution under C:\Projects\dynamics
- Set your build profile to Sandbox
- The Nuget Package Manager Console should start installing SCORE and other packages for you and if it doesn't then close VS 2017 and reopen the solution again or go to
- This process takes a few minutes. You can follow the setup steps through Visual Studio Package Manager Console
- Go to http://dynamics/score/about/version and make sure your license is valid

## Sync TDS Projects
Sync the projects in this order: dynamics.TDS.Core, dynamics.TDS.Master and dynamics.TDS.Master.Content

## Add dynamics Tenant Cores to Solr
SCORE needs 2 indexes that have to be added to Solr

- dynamics_tenant_master_index
- dynamics_tenant_web_index

The config files were added to the solution:
- ContentSearch.Solr.Index.Master.config
- ContentSearch.Solr.Index.Web.config

Solr doesn't know they exist so you have to add them as a core manually. You should create the following folders for 'dynamics_tenant_master_index'

 - C:\solr-6.6.2\server\solr\dynamics_tenant_master_index
 - C:\solr-6.6.2\server\solr\dynamics_tenant_master_index\data
 - Copy everything under C:\solr-6.6.2\server\solr\configsets\basic_configs and place it under C:\solr-6.6.2\server\solr\dynamics_tenant_master_index

Do the same for 'dynamics_tenant_web_index' and you should have the following
 - C:\solr-6.6.2\server\solr\dynamics_tenant_web_index
 - C:\solr-6.6.2\server\solr\dynamics_tenant_web_index\data
 - Copy everything under C:\solr-6.6.2\server\solr\configsets\basic_configs and place it under C:\solr-6.6.2\server\solr\dynamics_tenant_web_index

Once that is done then do the following:
 - Open Solr (https://localhost:8983/solr/#/)
 - Click on "Core Admin" on the left hand bar
 - Click on the button "Add Core"
   - Fill in the following parameters for dynamics_tenant_master_index
     - name: dynamics_tenant_master_index
     - instanceDir: dynamics_tenant_master_index
     - dataDir: data
     - config: solrconfig.xml
     - schema: schema.xml
  - Click the blue button "Add Core"
- Click on the button "Add Core"
  - Fill in the following parameters for dynamics_tenant_web_index
    - name: dynamics_tenant_web_index
    - instanceDir: dynamics_tenant_web_index
    - dataDir: data
    - config: solrconfig.xml
    - schema: schema.xml
  - Click the blue button "Add Core"
- Open the file C:\solr-6.6.2\server\solr\dynamics_tenant_master_index\conf\managed-schema and make the following changes
  - change `<uniqueKey>_uniqueid</uniqueKey>`
  - add `<field name="_indexname" type="string" indexed="true" stored="true"/>`
  - add `<field name="_uniqueid" type="string" indexed="true" required="true" stored="true"/>`
- Open the file C:\solr-6.6.2\server\solr\dynamics_tenant_web_index\conf\managed-schema and make the following changes
  - change `<uniqueKey>_uniqueid</uniqueKey>`
  - add `<field name="_indexname" type="string" indexed="true" stored="true"/>`
  - add `<field name="_uniqueid" type="string" indexed="true" required="true" stored="true"/>`
- Go to services in Windows and stop and start your Solr service "SOLR"
- Login to sitecore. Open Control Panel → Populate Solr Managed Schema → Select all → Populate. ( You should see dynamics_tenant_master_index and dynamics_tenant_web_index in the list to populate but if you don't make sure the solr config files are not disabled for both config files)
- After this completes, Click on Indexing manager and rebuild all indexes. Confirm that your newly created indexes are displaying in the Index list.
	  
## Build your solution
Using the Sandbox build configuration, build your solution

## Web.config updates
	Add this configuration to your web.config file
	  <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-12.0.0.0" newVersion="12.0.0.0" />
      </dependentAssembly>
	  
Congratulations you have now set up dynamics with Sitecore 9 and SCORE 3!

## Troubleshooting
- If you first set this up and run into an index issue it might be because the following files are currently enabled and you haven't set up the solr cores yet.   Rename them with .disabled at the end (ex: ContentSearch.Solr.Index.Master.config.disabled) and see if that fixes the issue.
  - C:\Projects\dynamics\sandbox\dynamics\App_Config\Include\dynamics\ContentSearch.Solr.Index.Master.config
  - C:\Projects\dynamics\sandbox\dynamics\App_Config\Include\dynamics\ContentSearch.Solr.Index.Web.config
- Make sure you installed Solr 6.6.2 and not Solr 6.6.1 because in some documentation it mentions you can use 6.6.1 but it causes a locking issue that 6.6.2 solves
- If you run into an issue with the Sitecore 9 install and need to uninstall it then do the following
  - Open Windows Powershell as Administrator
  - Type "cd C:\Projects\dynamics" (without the double quotes)
  - Run the powershell script below
    - This is a sample script so please change the variables values that fit your instance
```
.\Automation\Sitecore-Versions\9.0\uninstall.ps1 -Prefix "dynamics" -SolrService "SOLR" -PathToSolr "C:\solr-6.6.2\" -SqlServer "DESKTOP-VMCOPY\SQLSERVER2016" -SqlAccount "sa" -SqlPassword "<Your Password>"
```
- After running the uninstall script make sure all tables created for dynamics are deleted for instance 'dynamics_Messaging' might not be deleted and will cause issues when you do the install again
- If you are running into issues cloning the project through Source Tree you can try to:
  - Update to the latest version. I had to do that because the putty client I had was having a security error. More information [here](https://github.com/gitextensions/gitextensions/issues/4509).
  - Create a public SSH key using Source tree
  - Get the public key value and add to your Settings in GitHub.com profile that was enabled for the dynamics project
  - If your account has multi factor authentication enabled, you will need to set Personal Access Token. More information in this link https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line/
-If you are facing issues when pulling because of multi factor authentication you may need to generate a personal access token. More about it [here](https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line/)

## Install Microsoft Dynamics packages

URL for downloading the packages: https://dev.sitecore.net/Downloads/Dynamics_CRM_Connect/2x/Sitecore_Connect_for_Microsoft_Dynamics_365_for_Sales_210.aspx

Download and install Data Exchange Framework
Download and install SQL Provider for Data Exchange Framework
Download and install Sitecore Provider for Data Exchange Framework
Download and install xConnect Provider for Data Exchange Framework
Download and install Microsoft Dynamics 365 for Sales Provider for Data Exchange Framework
Download and install Sitecore Connect for Microsoft Dynamics 365 for Sales
Note: Sitecore Connect for Microsoft Dynamics 365 for Sales requires all other packages be installed. 