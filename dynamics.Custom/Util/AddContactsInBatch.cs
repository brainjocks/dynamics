﻿using System;
using System.Collections.Generic;
using dynamics.Custom.ExperienceActions;
using Sitecore.XConnect;
using Sitecore.XConnect.Client;
using Sitecore.XConnect.Collection.Model;

namespace dynamics.Custom.Util
{
    public class AddContactsInBatch
    {
        private const string CONTACT_LIST_ID = "b4ddd1f8-6d26-4c02-8b99-e5792a48f7c7";
        // Sync example
        public void ExampleSync()
        {
            using (Sitecore.XConnect.Client.XConnectClient client = Sitecore.XConnect.Client.Configuration.SitecoreXConnectClientConfiguration.GetClient())
            {
                try
                {
                    for (int i = 1; i <= 500; i++)
                    {
                        // Adds an AddContactOperation operation to the batch
                        var contact = new Sitecore.XConnect.Contact();

                        //client.SetFacet<PersonalInformation>(contact, new PersonalInformation() { FirstName = $"Sample Contact {i}" });
                        UpdateContactAction.SetPersonalInformation($"First{i}", $"Last{i}", contact, client);
                        UpdateContactAction.SetEmail($"user{i}@email.com", contact, client);
                        var listSubscription = new ContactListSubscription(DateTime.Now, true, new Guid(CONTACT_LIST_ID));
                        client.SetFacet(contact, new ListSubscriptions() { Subscriptions = new List<ContactListSubscription> {listSubscription}}
                            );
                        client.AddContact(contact); // Extension found in Sitecore.XConnect
                    }

                    // Submits the batch, which contains 1000 operations
                    // 500 AddContactOperation
                    // 500 SetFacetOperation
                    client.Submit();

                }
                catch (Sitecore.XConnect.XdbExecutionException ex)
                {
                    // Manage exception
                }
            }
        }
    }
}