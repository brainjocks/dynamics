using System.Web.Mvc;

namespace dynamics.Web.Areas.dynamics
{
    public class dynamicsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "dynamics";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            // Register your MVC routes in here
        }
    }
}
